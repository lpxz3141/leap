/**
 * 
 */
package edu.hkust.leap.utils;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;


public class ChordWrapper {
    // for object field or static field, it is awesome.
	// for array, it uses the form [*] to denote the the array (any kind of).
	// for hashmap, it does nothing. (it cannot analyze the jdk stuff too)
	
	public static String fieldMarker= "Dataraces on";
	 public static HashMap<String, Set<String>> field2RaceMethods = new HashMap<String, Set<String>>();

	 public static boolean isFalseSharing(String field, String classMethodName)
	 {
		 if(!chordEnabled)
			 return false; // I cannot say that it is false sharing. false sharing is a must information.
			 
		 Set<String> raceMethods = field2RaceMethods.get(field);
		 return !(raceMethods!=null&&raceMethods.contains(classMethodName));		 // thos that are races are included.
	 }
	 public static void reportField2RaceMethods()
	 {
		 for(String key:field2RaceMethods.keySet()) 
	        {
	        	System.out.println("\nfield: " + key);
	        	Set<String> raceMethods = field2RaceMethods.get(key);
	        	for(String raceMethod : raceMethods)
	        	{
	        		System.out.print(raceMethod +' ' );
	        	}
	        }
	        
	        System.out.println();
	 }
     
     public static boolean chordEnabled =true;
     
	 public static void fetchField2RaceMethods(String argfileName )throws Exception
	 {
		       if(argfileName==null || argfileName.trim().isEmpty() )
		       {
		    	   chordEnabled =false;
		           return;
		       }
		       
		       if( !(new File(argfileName).exists()))
		    	   throw new RuntimeException("the file specified does not exist, check it");
		       
		       
		       
		        String latestField = null;
	    	    String toret  = "";
	    	    String strLine="";
			  
			    FileInputStream fstream = new FileInputStream(argfileName);
			    DataInputStream in = new DataInputStream(fstream);
			        BufferedReader br = new BufferedReader(new InputStreamReader(in));
			  
			    //Read File Line By Line
			        System.err.println("loading chord..................");
			        
			    
			    while ((strLine = br.readLine()) != null)   {
			    	if (strLine.isEmpty()||strLine.trim().isEmpty()) {
						continue;
					}
			    	try {
			    		if(strLine.contains(fieldMarker))
		                   {
		                	   String field = getField(strLine);
		                	   Set<String> raceMethods = field2RaceMethods.get(field); 
		                	   if(raceMethods==null)
		                	   {
		                		   raceMethods = new HashSet<String>();
		                		   field2RaceMethods.put(field, raceMethods);
		                	   }
		                	   latestField= field;
		                   }else {
							    if(latestField==null) continue; // not even reaching the first section.
							    Set<String> tmpRaceMethods = getRaceMethods(strLine);
							    field2RaceMethods.get(latestField).addAll(tmpRaceMethods);               	   
						    }       
					} catch (Exception e) {
						// TODO: handle exception}catch (Exception e){//Catch exception if any
				    	e.printStackTrace();
				    	System.out.println("here:"+strLine.trim());
				    	
				      System.err.println("Error: " + e.getMessage() + "\n" + strLine+"\n");
				    
					}
                        
			   }
			    //Close the input stream
			    in.close();
			    
			    for(String key:field2RaceMethods.keySet())
			    {
			    	System.out.println("\nfield: " + key);
			    	Set<String> methods= field2RaceMethods.get(key);
			    	for(String method:methods)
			    	{
			    		System.out.println(method);
			    		
			    	}
			    }
			
			
	    	
	 }
	 

//	 1.1	hedc.PooledExecutorWithInvalidate$Worker.run()	hedc.MetaSearchRequest.countDownInterrupt() (Rd)	hedc.Tester.run()	hedc.MetaSearchRequest.registerInterrupt(java.lang.Thread,int) (Wr)
	 public static HashSet<String > tmpset = new HashSet<String>();
	 private static Set<String> getRaceMethods(String strLine) {
		 tmpset.clear();
		 
        String[] tokens= strLine.split("\\(");
        String firstString=tokens[1];
        int index = firstString.indexOf(')');
        firstString = firstString.substring(index+1);
        
        String secondString = tokens[4];
        index= secondString.indexOf(')');
        secondString = secondString.substring(index+1);
        
        
        tmpset.add(firstString.trim());
        tmpset.add(secondString.trim());
        
		return tmpset;
	}


//	1. Dataraces on hedc.MetaSearchRequest.thread_	
	private static String getField(String strLine) {
        int starter=  strLine.indexOf( fieldMarker);
        strLine=strLine.substring(starter + fieldMarker.length());        
		return strLine.trim().intern();
	}

	public static String dataracesFileName = "dataraces_by_fld.txt";
	public static String chordBenchmarkDir = "/home/lpxz/work/chord/benchmarks";
	public static void setupChordResults(String chordDir)
	{
		List<File> files = new ArrayList<File>(); 
		File dir = new File(chordDir);
	    finder(files, dir);
	    
		for(File file: files)
		{
			try {
				fetchField2RaceMethods(file.getAbsolutePath());			
			} catch (Exception e) {
				// TODO: handle exception
			}
			
		}
	}
	
	public static List<File> finder(List<File> files, File dir)
	{
	    if (files == null)
	        files = new LinkedList<File>();

	    if (!dir.isDirectory())
	    {
	    	if(dir.getName().equals(dataracesFileName))
	        files.add(dir);
	        return files;
	    }

	    for (File file : dir.listFiles())
	    	finder(files, file);
	    return files;
	}
	
	public static String arrayAnnotationOfChord = "[*]";
	
	public static void main(String[] args) {
//		fetchField2RaceMethods("/home/lpxz/work/chord/benchmarks/sunflow/chord_output/dataraces_by_fld.txt");
////		setupChordResults(chordBenchmarkDir);
//		boolean result =isFalseSharing(arrayAnnotationOfChord, "test.A.get");
//		System.out.println(result);
		
	}

}
