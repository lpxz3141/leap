package edu.hkust.leap.record;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.zip.GZIPOutputStream;

import edu.hkust.leap.monitor.RecordMonitor;
import edu.hkust.leap.record.generator.CrashTestCaseGenerator;
import edu.hkust.leap.record.utils.Serializer;
import edu.hkust.leap.record.utils.Util;
import gnu.trove.list.linked.TIntLinkedList;
import gnu.trove.list.linked.TLongLinkedList;

public class MonitorThread extends Thread
{
	
	
	public MonitorThread()
	{
		super("MonitorThread");
	}
	public void run()
	{

		
//		synchronized (RecordMain.mainThreadLock)
		{
			   System.out.println("record run takes: " + (System.currentTimeMillis()- RecordMonitor.start));
				
				if(RecordMonitor.leap)
				{							
					int leap= RecordMonitor.checkpointedLeap+MonitorThread.sizeOfDataSingleDimColl(RecordMonitor.accessVectorGroup);
					System.out.println("leapTotal: " + leap);	
					
//					System.out.println("maximal index"+ max(RecordMonitor.accessVectorGroup));
					
					
				}
				else if (RecordMonitor.stride) {
					int leappart= RecordMonitor.checkpointedLeap+MonitorThread.sizeOfDataSingleDimColl(RecordMonitor.accessVectorGroup);
					int stridepart=(RecordMonitor.checkpointedStride+(MonitorThread.sizeOfDataSingleDimColl(RecordMonitor.perThreadGroup)))/2;// 2 ints as 1 long
					int total = leappart + stridepart;
					System.out.println("strideTotal: " + total );	
				}
				else if(RecordMonitor.myBasic){
					// no need for checkpointing, no need to adjustment. NB
					int leappart= RecordMonitor.checkpointedLeap+ MonitorThread.sizeOfDataSingleDimColl(RecordMonitor.accessVectorGroup);
					int mypart= 0;
					if(RecordMonitor.opt_avoid_autoboxing)
					{
						mypart=RecordMonitor.checkpointedMy+MonitorThread.sizeOfDataDoubleDimColl_TLinkList(RecordMonitor.myAccessVectorGroup_Key) ; 
					}else {
						mypart=RecordMonitor.checkpointedMy+MonitorThread.sizeOfDataDoubleDimColl_LinkedMap(RecordMonitor.myAccessVectorGroup) ; 
					}
					
					int total= leappart + mypart;
					System.out.println("myTotal: " + total );	
				}
		   }
		 
		 
		 if(RecordMonitor.statistic_hotspot_underLeap)
			{					
				statisticHotspot();
			}
		 
		 
		 
		 
//		   synchronized (RecordMain.mainThreadLock) {
//			   System.out.println("record run takes: " + (System.currentTimeMillis()- RecordMonitor.start));
//				
//				if(RecordMonitor.leap)
//				{
//					System.out.println("leapTotal: " + sizeOfDataSingleDimColl(RecordMonitor.accessVectorGroup) );	
//				}
//				else if (RecordMonitor.stride) {
//					System.out.println("strideTotal: " + (sizeOfDataSingleDimColl(RecordMonitor.accessVectorGroup)+(sizeOfDataSingleDimColl(RecordMonitor.perThreadGroup))/2 ) );	
//				}
//				else if(RecordMonitor.myBasic){
//					System.out.println("myTotal: " + (sizeOfDataSingleDimColl(RecordMonitor.accessVectorGroup)+ sizeOfDataDoubleDimColl(RecordMonitor.myAccessVectorGroup_Key) ) );	
//				}
//		   }
		
		
		
//		System.out.println("opt times: " + RecordMonitor.optTimes);
		
		
//		for(int iid =0; iid<RecordMonitor.accessVectorGroup.length; iid++)
//		{
//			
//			Vector v = RecordMonitor.accessVectorGroup[iid];
//			
//			if(v.size()==0) continue;
//			System.out.println("\nfor this loc" + iid);
//			
//            Iterator it =v.iterator();
//		    while (it.hasNext()) {
//				Object object = (Object) it.next();
//				System.out.print(object +" ");				
//			}
//		}
		
		
		
//		saveMonitorData();
		
		
//		for(int i=0;i<	RecordMonitor.myAccessVectorGroup.length; i++)
//		{
//			LinkedHashMap[] ith = RecordMonitor.myAccessVectorGroup[i];
//			for(int j=0; j< ith.length; j++)
//			{
//				LinkedHashMap map = ith[j];
//				System.out.println("map's size for T " + i+ " " + j + " :" + map.size() );
//				
//				
//			}
//		}
			
//		if(RecordMonitor.isCrashed)
//		{
//			System.err.println("--- program crashed! ---");
//			System.err.println("--- preparing for reproducing the crash ... ");
//			String traceFile_ = saveMonitorData();
//			System.err.println("--- generating the test driver program ... ");
//			generateTestDriver(traceFile_);
//		}
//		else
//		{
//			saveMonitorData();
////			generateTestDriver(saveMonitorData());
//		}
	   

	    
	    
	}
	

	
	/**
	 * 
	 */
	public static void statisticHotspot() {

		System.out.println("\n" + "top 10 hot methods:");
		ConcurrentHashMap<String, AtomicInteger> hotspotMethods = RecordMonitor.hotspotMethods;
		if(hotspotMethods.size()>0){
			
			     List list = new LinkedList(hotspotMethods.entrySet());
			     Collections.sort(list, new Comparator() {
			    	 
			          public int compare(Object o1, Object o2) {
			               return 
			            		((AtomicInteger)   ((Map.Entry) (o2)).getValue()).get()-  ((AtomicInteger)(((Map.Entry) (o1)).getValue())).get();
			               
			          }
			     });

			    Map result = new LinkedHashMap();
			    int i=0;
			    for (Iterator it = list.iterator(); it.hasNext();) {
			        Map.Entry entry = (Map.Entry)it.next();
			        System.out.println(entry.getKey() + " " +  entry.getValue());				        
//			        result.put(entry.getKey(), entry.getValue());
			        i++;
			        if(i>10) // top 10
			        	break;
			    }
		}
		
	}

	
	
	 /**
	 * @param myAccessVectorGroup_Key
	 * @return
	 */
	
//	for(int i=0 ; i< threadSize; i++)
//	{
//		for(int j=0; j< accessedLocSize; j++)
//		{
//			myAccessVectorGroup_Key[i][j]= new TLongLinkedList();
//		}
//	}
	public static int sizeOfDataDoubleDimColl_TLinkList(TLongLinkedList[][] myAccessVectorGroup_Key) {
		int size=0;
        for(int i=0; i<myAccessVectorGroup_Key.length; i++)
        {
        	TLongLinkedList[] tmp= myAccessVectorGroup_Key[i];
        	for(int j=0; j<tmp.length; j++)
        	{
        		size+=tmp[j].size();
        	}
        }
        return size;
	}
	
	/**
	 * @param myAccessVectorGroup
	 * @return
	 */
	public static int sizeOfDataDoubleDimColl_LinkedMap(LinkedHashMap[][] myAccessVectorGroup) {
		int size=0;
        for(int i=0; i<myAccessVectorGroup.length; i++)
        {
        	LinkedHashMap[] tmp= myAccessVectorGroup[i];
        	for(int j=0; j<tmp.length; j++)
        	{
        		size+=(tmp[j].size()*2);// two elements per entry.
        	}
        }
        return size;
	}
	
	
	/**
	 * @param accessVectorGroup
	 * @return
	 */
	public static int sizeOfDataSingleDimColl(Vector[] accessVectorGroup) {
		int size= 0; 
		
		
		for(int i=0 ; i<accessVectorGroup.length;i++)
		{
			size+=accessVectorGroup[i].size();
		}
		return size;
	}
	
	public static int max(Vector[] accessVectorGroup) {
		int size= 0; 
		
		int maxvalue= -1;
		int maxIndex = -1; 
		
		for(int i=0 ; i<accessVectorGroup.length;i++)
		{
			if(accessVectorGroup[i].size()>maxvalue)
			{
				maxvalue=accessVectorGroup[i].size();
				maxIndex=i;
			}
		}
		return maxIndex;
	}
	
	
	public static int sizeOfDataSingleDimColl_LinkedList(TIntLinkedList[] accessVectorGroup) {
		int size= 0; 
		
		
		for(int i=0 ; i<accessVectorGroup.length;i++)
		{
			size+=accessVectorGroup[i].size();
		}
		return size;
	}
	
	public static String saveMonitorData()
	    {
	    	String traceFile_=null;
			File traceFile_monitordata = null;
			File traceFile_threadNameToIdMap= null;
//			File traceFile_nanoTimeDataVec = null;
//			File traceFile_nanoTimeThreadVec = null;
//			
			OutputStreamWriter fw_monitordata;
			OutputStreamWriter fw_threadNameToIdMap;
//			OutputStreamWriter fw_nanoTimeDataVec;
//			OutputStreamWriter fw_nanoTimeThreadVec;
//			
			//SAVE Runtime Information
			try 
			{
				traceFile_monitordata = File.createTempFile("Leap", "_accessVector.trace.gz", new File(
						Util.getOrderDataDirectory()));
				
				String traceFileName = traceFile_monitordata.getAbsolutePath();
				int index  =traceFileName.indexOf("_accessVector");
				traceFile_ = traceFileName.substring(0, index);
				
				traceFile_threadNameToIdMap = new File(traceFile_+"_threadNameToIdMap.trace.gz");
				String traceMapFileName = traceFile_threadNameToIdMap.getAbsolutePath();

//				
				
				assert (traceFile_monitordata != null && traceFile_threadNameToIdMap != null);



//				
				Serializer.storeObject(RecordMonitor.accessVectorGroup, traceFileName);
				Serializer.storeObject(RecordMonitor.threadNameToIdMap, traceMapFileName);

//				
			} catch (IOException e) {
				e.printStackTrace();
			}
			return traceFile_;
	    }
	    public static void generateTestDriver(String traceFile_)
	    {
			//GENERATE Test Driver		
			try {
				CrashTestCaseGenerator.main(new String[] { traceFile_,
						Util.getReplayDriverDirectory() });
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }
		
}	
