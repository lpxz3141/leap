package edu.hkust.leap.record;

import java.io.FileInputStream;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;




import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import edu.hkust.leap.monitor.RecordMonitor;
import edu.hkust.leap.record.utils.Util;

public class RecordMain {

	// Note that to change the buildpath!!
	// NOTE to change the CrashTestCaseGenerator. invoke main(): LP version
	public static void main(String[] args) {

		
		// for regular benchmarks, not the dacapo.
		String argfileName= Util.getRecordArgFile();
		System.out.println(argfileName);
		
		String argline= Util.getArgLine(argfileName);
		String[]  arglineItems = argline.split(" ");
		
		List<String>  arg = new LinkedList(Arrays.asList(arglineItems));
		int len = arg.size();
		if(len==0)
		{
			System.err.println("please specify SPE size, the main class, and parameters... ");
		}
		else 
		{
//			process(arg);
			run(arg);
			
		}
	}
			
//	private static void process(List<String> args)
//	{
////		int index=0;
//
////			RecordMonitor.initialize(Integer.valueOf(args.get(0)));
////			run(args.subList(++index, args.size()));
//	}
	
	public static Object mainThreadLock= new Object();

	private static void run(List<String> args) 
	{
		try 
		{
			String arglineFileName = Util.getRecorderArgFile();
			
			String argline =Util.getArgLine(arglineFileName).trim();
	        
			int index = argline.lastIndexOf('/');
			String currentFolder = argline.substring(0, index);
			
			Properties properties = new Properties();
           
	        try {
	            properties.load(new FileInputStream(
	            		argline  ));
	        } catch (Exception e) {
	            System.out.println("Exception Occurred" + e.getMessage());
	        }    	
	        
	        	        
	        String mainClass = properties.getProperty("main").trim();
	        String argsString = null;
	        try {
				argsString=properties.getProperty("args").trim();
			} catch (Exception e) {
				// TODO: handle exception
			}
	        		
	        
			MonitorThread monThread = new MonitorThread();
			Runtime.getRuntime().addShutdownHook(monThread);
			String[] mainArgs = {};
			// format: 1 appclass appclass_main_argments
		
			String appname = mainClass;//args.get(0);
			Class<?> c = Class.forName(appname);
		    Class[] argTypes = new Class[] { String[].class };
		    Method main = c.getDeclaredMethod("main", argTypes);
		    if(!main.isAccessible())
		    {
		    	main.setAccessible(true);
		    }
		   
		    if(argsString!=null)
		    {
		    	mainArgs = argsString.split(" ");
		    }
		    
//		    synchronized (mainThreadLock)
		    {
		    	   main.invoke(null, (Object)mainArgs);
			}
		 
		  // analysis here:
		    
		    	    

		    
			// production code should handle these exceptions more gracefully
			} catch (Exception x) {            
			    x.printStackTrace();
			}finally
			{
				 
					
				 
			}
	}

	
}
