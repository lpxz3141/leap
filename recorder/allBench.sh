#raytracer
cp ./args/leap.recorder.arg.raytracer leap.recorder.arg
./aBench.sh raytracer

#montecarlo
cp ./args/leap.recorder.arg.montecarlo leap.recorder.arg
./aBench.sh montecarlo

#moldyn
cp ./args/leap.recorder.arg.moldyn leap.recorder.arg
./aBench.sh moldyn

#bayes
cp ./args/leap.recorder.arg.bayes leap.recorder.arg
./aBench.sh bayes

#intruder
cp ./args/leap.recorder.arg.intruder leap.recorder.arg
./aBench.sh intruder


#kmeans
cp ./args/leap.recorder.arg.kmeans leap.recorder.arg
./aBench.sh kmeans

#laby
cp ./args/leap.recorder.arg.laby leap.recorder.arg
./aBench.sh laby

#matrix
cp ./args/leap.recorder.arg.matrix leap.recorder.arg
./aBench.sh matrix

#ssca2
cp ./args/leap.recorder.arg.ssca2 leap.recorder.arg
./aBench.sh ssca2

#vacation
cp ./args/leap.recorder.arg.vacation leap.recorder.arg
./aBench.sh vacation

#yada
cp ./args/leap.recorder.arg.yada leap.recorder.arg
./aBench.sh yada

#cache4j
cp ./args/leap.recorder.arg.cache4j leap.recorder.arg
./aBench.sh cache4j

#hedc
cp ./args/leap.recorder.arg.hedc leap.recorder.arg
./aBench.sh hedc


#weblech
cp ./args/leap.recorder.arg.weblech leap.recorder.arg
./aBench.sh weblech

#jgrapht
cp ./args/leap.recorder.arg.jgrapht leap.recorder.arg
./aBench.sh jgrapht

#specjbb
cp ./args/leap.recorder.arg.specjbb leap.recorder.arg
./aBench.sh specjbb

#ftpserver, problematic
#cp ./args/leap.recorder.arg.ftpserver leap.recorder.arg
#./aBench.sh ftpserver

#jigsaw
cp ./args/leap.recorder.arg.jigsaw leap.recorder.arg
./aBench.sh jigsaw

#tomcat
cp ./args/leap.recorder.arg.tomcat leap.recorder.arg
./aBench.sh tomcat

#batik
cp ./args/leap.recorder.arg.batik leap.recorder.arg
./aBenchDacapo.sh batik

#luindex
cp ./args/leap.recorder.arg.luindex leap.recorder.arg
./aBenchDacapo.sh luindex

#lusearch
cp ./args/leap.recorder.arg.lusearch leap.recorder.arg
./aBenchDacapo.sh lusearch


#sunflow
cp ./args/leap.recorder.arg.sunflow leap.recorder.arg
./aBenchDacapo.sh sunflow

#xalan
cp ./args/leap.recorder.arg.xalan leap.recorder.arg
./aBenchDacapo.sh xalan



